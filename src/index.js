import React from 'react';
import ReactDOM from 'react-dom';

/** Router */
import App from './App';

const title = 'STARTER PACK BY MEITOEY';

ReactDOM.render(
  <App />,
  document.getElementById('app') 
);

module.hot.accept();
